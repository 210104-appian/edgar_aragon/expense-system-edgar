# Expense System

A Full-Stack clone about solving expense reposrts on company time.

## About This Project

The goal of this project was to keep track of a comapny's expense repoerts made by the employees. When employees travel, they are bound to encounter expenses and keeping track of these expense reports can be hard. Especially considering the amount of employees that a comapny can have. This Expense System would help companies keep track of each report and even speed up their process until they get back to an employee. In addition to speed, the system gurantees less errors overall since the system itself transfers reports between other members or groups.

## How Does It Work

The application is comprised of a REST API built in Spring Tools using Maven and ran on the Apache Tomcat server. The application pulls data from a database hosted on AWS RDS in Postgres using DBeaver.

The client is directed to user-centered webpages composed of HTML, CSS, Bootstrap, and Javascript to present the visuals of the application.

## Notes For Contributing

## Technologies

Project 1's backend consists of a Java application built using:
- Maven
- Spring Tools Suite
- DBeaver
- JUnit
- JDBC

The Technologies used for the Front end include:
- Javascript
- HTML/CSS
- [Bootstrap 5](https://getbootstrap.com/docs/5.0/getting-started/introduction/)
- Servlets

## Notes On The Architecure
### How To Get Started

The following instructions are for setting up the Java web application on a Tomcat v9.0 server in Spring Tool Suite 4:

- Clone the project's Git repository in a directory of your choosing using the following Git command: git clone https://gitlab.com/210104-appian/edgar_aragon/expense-system-edgar.git.

- Download and extract the binary distribution zip file for Apache Tomcat v9.0 in a directory of your choosing from the following webpage: Apache Tomcat 9.

- Within the IDE, Right-click in the Package Explorer tab, select Import > Maven > Existing Maven Projects, click on Browse, select the project folder you cloned named ers-app, and click Finish to import the Maven project.

- Right-click the newly imported project folder named ers-app and select Maven > Update Project to update the Maven project and download any required dependencies from the pom.xml file.

- In the menu bar, go to Window > Show View > Other, select Servers, and click Open to display the server view tab.

- Right-click on the server view and a pop-up menu will be appear. From there, choose New > Server.

- Choose Apache > Tomcat v9.0 and click on Next.

- Click on Browse, select the path location where the extracted version of Tomcat v9.0 exists, and click on Finish to create the server.

- To add the Java web application to the server, right-click the Tomcat v9.0 Server at localhost, select Add and Remove, add the ers-app as a web module, and click Finish.

- Finally, run the server by right-clicking it and selecting Start.

- To theoretically set up the connection to the Oracle Database, go to Run > Run Configurations in the menu bar and set up the Environment Variables for the Tomcat v9.0 Server at localhost to include variables for the DB_PASSWORD, DB_URL, and DB_USERNAME. This allows for a secure connection to the database without needing to hardcode the database's URL and login credentials. NOTE: The Oracle Database used initially for this project no longer exists and will instead be replaced with static data within the Java application.


