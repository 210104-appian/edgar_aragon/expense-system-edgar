package expense.system.Aragon.Model;

public class ReimbursementRequest {

	private int requestId;
	private float requestAmount;
	private String paidToFirstName;
	private String paidToLastName;
	private String reimbursedOn;
	private String paidThrough;
	private int referenceNumber;
	private String status;
	private String approvedByFirstName;
	private String approvedByLastName;
	private int employeeId;
	
	public ReimbursementRequest() {
		super();
	}
	
	public ReimbursementRequest(int requestId, float requestAmount, String paidToFirstName, String paidToLastName,
			String reimbursedOn, String paidThrough, int referenceNumber, String status, String approvedByFirstName,
			String approvedByLastName, int employeeId) {
		super();
		this.requestId = requestId;
		this.requestAmount = requestAmount;
		this.paidToFirstName = paidToFirstName;
		this.paidToLastName = paidToLastName;
		this.reimbursedOn = reimbursedOn;
		this.paidThrough = paidThrough;
		this.referenceNumber = referenceNumber;
		this.status = status;
		this.approvedByFirstName = approvedByFirstName;
		this.approvedByLastName = approvedByLastName;
		this.employeeId = employeeId;
	}

	public ReimbursementRequest(float requestAmount, String paidToFirstName, String paidToLastName, String reimbursedOn,
			String paidThrough, String status, String approvedByFirstName, String approvedByLastName, int employeeId) {
		super();
		this.requestAmount = requestAmount;
		this.paidToFirstName = paidToFirstName;
		this.paidToLastName = paidToLastName;
		this.reimbursedOn = reimbursedOn;
		this.paidThrough = paidThrough;
		this.status = status;
		this.approvedByFirstName = approvedByFirstName;
		this.approvedByLastName = approvedByLastName;
		this.employeeId = employeeId;
	}

	public int getRequestId() {
		return requestId;
	}
	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}
	public float getRequestAmount() {
		return requestAmount;
	}
	public void setRequestAmount(float requestAmount) {
		this.requestAmount = requestAmount;
	}
	public String getPaidToFirstName() {
		return paidToFirstName;
	}
	public void setPaidToFirstName(String paidToFirstName) {
		this.paidToFirstName = paidToFirstName;
	}
	public String getPaidToLastName() {
		return paidToLastName;
	}
	public void setPaidToLastName(String paidToLastName) {
		this.paidToLastName = paidToLastName;
	}
	public String getReimbursedOn() {
		return reimbursedOn;
	}
	public void setReimbursedOn(String reimbursedOn) {
		this.reimbursedOn = reimbursedOn;
	}
	public String getPaidThrough() {
		return paidThrough;
	}
	public void setPaidThrough(String paidThrough) {
		this.paidThrough = paidThrough;
	}
	public int getReferenceNumber() {
		return referenceNumber;
	}
	public void setReferenceNumber(int referenceNumber) {
		this.referenceNumber = referenceNumber;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getApprovedByFirstName() {
		return approvedByFirstName;
	}
	public void setApprovedByFirstName(String approvedByFirstName) {
		this.approvedByFirstName = approvedByFirstName;
	}
	public String getApprovedByLastName() {
		return approvedByLastName;
	}
	public void setApprovedByLastName(String approvedByLastName) {
		this.approvedByLastName = approvedByLastName;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	@Override
	public String toString() {
		return "ReimbursementRequest [requestId=" + requestId + ", requestAmount=" + requestAmount
				+ ", paidToFirstName=" + paidToFirstName + ", paidToLastName=" + paidToLastName + ", reimbursedOn="
				+ reimbursedOn + ", paidThrough=" + paidThrough + ", referenceNumber=" + referenceNumber + ", status="
				+ status + ", approvedByFirstName=" + approvedByFirstName + ", approvedByLastName=" + approvedByLastName
				+ ", employeeId=" + employeeId + "]";
	}
	
}
