package expense.system.Aragon.Model;

public class Employee {
	
	private int employeeId;
	private String username;
	private String password;
	private String email;
	private String country;
	private String phoneNumber;
	
	public Employee(){
		super();
	}
	
	public Employee(int employeeId, String username, String password, String email, String country,
			String phoneNumber) {
		super();
		this.employeeId = employeeId;
		this.username = username;
		this.password = password;
		this.email = email;
		this.country = country;
		this.phoneNumber = phoneNumber;
	}
	
	public Employee(String username, String password, String email, String country,
			String phoneNumber) {
		super();
		this.username = username;
		this.password = password;
		this.email = email;
		this.country = country;
		this.phoneNumber = phoneNumber;
	}

	public int getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Override
	public String toString() {
		return "Employee [employeeId=" + employeeId + ", username=" + username
				+ ", password=" + password + ", email=" + email + ", country=" + country + ", phoneNumber="
				+ phoneNumber + "]";
	}
}
