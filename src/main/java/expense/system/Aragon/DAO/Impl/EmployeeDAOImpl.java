package expense.system.Aragon.DAO.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import expense.system.Aragon.DAO.EmployeeDAO;
import expense.system.Aragon.Model.Employee;
import expense.system.Aragon.Util.ConnectionUtil;

public class EmployeeDAOImpl implements EmployeeDAO {

	//This method verifies the user's first name, last name, and password to enter the system
	@Override
	public Employee VerifyNameAndPassword(String username, String password) {
		String sqlu = "SELECT FIRST_NAME, LAST_NAME, PASSWORD FROM EMPLOYEE_INFO WHERE USERNAME = ? AND PASSWORD = ?";
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sqlu);){
			
			pStatement.setString(1, username);
			pStatement.setString(3, password);
			
			ResultSet resultSet = pStatement.executeQuery();
			
			Employee employee = new Employee();
			employee.setUsername(username);
			employee.setPassword(password);
			
			if(resultSet.next())
			{
				return employee;
			}
			else
			{
				return null;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	//This method gets ALL the employees and their information
	@Override
	public List<Employee> GetAllEmployees() {
		String sql = "SELECT * FROM EMPLOYEE_INFO";
		List<Employee> employees = new ArrayList<>();
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement();){
			ResultSet rs = statement.executeQuery(sql);
			while(rs.next()) {
				Employee employee = new Employee();
				
				employee.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				employee.setUsername(rs.getString("USERNAME"));
				employee.setPassword(rs.getString("PASSWORD"));
				employee.setEmail(rs.getString("EMAIL"));
				employee.setCountry(rs.getString("COUNTRY"));
				employee.setPhoneNumber(rs.getString("PHONE_NUMBER"));
				
				employees.add(employee);
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employees;
	}

	//This method gets all the information of one employee and is searched by the employee's ID number
	@Override
	public Employee GetEmployeeInfoByID(int employeeID) {
		Employee employee = new Employee();
		String sql = ("SELECT * FROM EMPLOYEE_INFO WHERE EMPLOYEE_ID = ?");
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql);){
						
			preparedStatement.setInt(1, employeeID);
			ResultSet rs = preparedStatement.executeQuery();
			
			while(rs.next()) {
				
				employee.setEmployeeId(employeeID);
				employee.setUsername(rs.getString("USERNAME"));
				employee.setPassword(rs.getString("PASSWORD"));
				employee.setEmail(rs.getString("EMAIL"));
				employee.setCountry(rs.getString("COUNTRY"));
				employee.setPhoneNumber(rs.getString("PHONE_NUMBER"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return employee;
	}

	//This method updates the employee's first name
	@Override
	public boolean UpdateUsername(String username, int employeeId) {
		String sql = "UPDATE EMPLOYEE_INFO SET USERNAME = ? WHERE EMPLOYEE_ID = ?";
		int activeUpdated = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)){
			pStatement.setString(1, username);
			pStatement.setInt(2, employeeId);
			activeUpdated = pStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(activeUpdated == 1) {
			return true;
		}
		return false;
	}

	//This method updates the employee's password
	@Override
	public boolean UpdatePassword(String password, int employeeId) {
		String sql = "UPDATE EMPLOYEE_INFO SET PASSWORD = ? WHERE EMPLOYEE_ID = ?";
		int activeUpdated = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)){
			pStatement.setString(1, password);
			pStatement.setInt(2, employeeId);
			activeUpdated = pStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(activeUpdated == 1) {
			return true;
		}
		return false;
	}

	//This method updates the employee's email
	@Override
	public boolean UpdateEmail(String email, int employeeId) {
		String sql = "UPDATE EMPLOYEE_INFO SET EMAIL = ? WHERE EMPLOYEE_ID = ?";
		int activeUpdated = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)){
			pStatement.setString(1, email);
			pStatement.setInt(2, employeeId);
			activeUpdated = pStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(activeUpdated == 1) {
			return true;
		}
		return false;
	}

	//This method updates the country that the employee lives in
	@Override
	public boolean UpdateCountry(String country, int employeeId) {
		String sql = "UPDATE EMPLOYEE_INFO SET COUNTRY = ? WHERE EMPLOYEE_ID = ?";
		int activeUpdated = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)){
			pStatement.setString(1, country);
			pStatement.setInt(2, employeeId);
			activeUpdated = pStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(activeUpdated == 1) {
			return true;
		}
		return false;
	}

	//This method updates the employee's phone number
	@Override
	public boolean UpdatePhoneNumber(String phoneNumber, int employeeId) {
		String sql = "UPDATE EMPLOYEE_INFO SET PHONE_NUMBER = ? WHERE EMPLOYEE_ID = ?";
		int activeUpdated = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)){
			pStatement.setString(1, phoneNumber);
			pStatement.setInt(2, employeeId);
			activeUpdated = pStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(activeUpdated == 1) {
			return true;
		}
		return false;
	}

}
