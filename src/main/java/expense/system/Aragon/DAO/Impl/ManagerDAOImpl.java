package expense.system.Aragon.DAO.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import expense.system.Aragon.DAO.ManagerDAO;
import expense.system.Aragon.Model.Manager;
import expense.system.Aragon.Util.ConnectionUtil;

public class ManagerDAOImpl implements ManagerDAO {

	@Override
	public boolean VerifyUsernameAndPassword(String username, String password) {
		String sqlu = "SELECT FIRST_NAME, LAST_NAME, PASSWORD FROM MANAGER_INFO WHERE USERNAME = ? AND PASSWORD = ?";
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement pStatement = connection.prepareStatement(sqlu);){
			
			pStatement.setString(1, username);
			pStatement.setString(3, password);
			
			ResultSet resultSet = pStatement.executeQuery();
			
			if(resultSet.next())
			{
				return true;
			}
			else
			{
				return false;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public Manager GetManagerInfoByID(int managerID) {
		Manager manager = new Manager();
		String sql = ("SELECT * FROM MANAGER_INFO WHERE MANAGER_ID = ?");
		try (Connection connection = ConnectionUtil.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql);){
						
			preparedStatement.setInt(1, managerID);
			ResultSet rs = preparedStatement.executeQuery();
			
			while(rs.next()) {
				
				manager.setManagerId(managerID);
				manager.setUsername(rs.getString("USERNAME"));
				manager.setPassword(rs.getString("PASSWORD"));
				manager.setEmail(rs.getString("EMAIL"));
				manager.setCountry(rs.getString("COUNTRY"));
				manager.setPhoneNumber(rs.getString("PHONE_NUMBER"));
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return manager;
	}

}
