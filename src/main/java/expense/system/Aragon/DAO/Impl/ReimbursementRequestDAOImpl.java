package expense.system.Aragon.DAO.Impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import expense.system.Aragon.DAO.ReimbursementRequestDAO;
import expense.system.Aragon.Model.ReimbursementRequest;
import expense.system.Aragon.Util.ConnectionUtil;

public class ReimbursementRequestDAOImpl implements ReimbursementRequestDAO {

	//This method submits or inserts a new request into the database
	@Override
	public ReimbursementRequest SubmitRequest(ReimbursementRequest rr) {
		String sql = "INSERT INTO REIMBURSEMENT_REQUEST_INFO (REQUEST_AMOUNT, PAID_TO_FIRST_NAME, PAID_TO_LAST_NAME, REIMBURSED_ON, PAID_THROUGH, STATUS, APPROVED_BY_FIRST_NAME, APPROVED_BY_LAST_NAME, EMPLOYEE_ID) "
				+ "VALUES (?,?,?,?,?,?,?,?,?)";
		ReimbursementRequest requestSubmited = rr;
		boolean success = false;
		
		try(Connection connection = ConnectionUtil.getConnection();
			PreparedStatement preparedStatement = connection.prepareStatement(sql);){
				
			preparedStatement.setFloat(1, rr.getRequestAmount());
			preparedStatement.setString(2, rr.getPaidToFirstName());
			preparedStatement.setString(3, rr.getPaidToLastName());
			preparedStatement.setString(4, rr.getReimbursedOn());
			preparedStatement.setString(5, rr.getPaidThrough());
			preparedStatement.setString(6, rr.getStatus());
			preparedStatement.setString(7, rr.getApprovedByFirstName());
			preparedStatement.setString(8, rr.getApprovedByLastName());
			preparedStatement.setInt(9, rr.getEmployeeId()); 
			success = preparedStatement.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		} 
		if(success)
		{
			return requestSubmited;
		}else
		{
			return null;
		}
	}

	//This method gets all the pending requests that a specific employee has
	@Override
	public List<ReimbursementRequest> GetAllPendingRequestsByEmployeeId(int employeeId) {
		
		String sql = "SELECT * FROM REIMBURSEMENT_REQUEST_INFO WHERE EMPLOYEE_ID = ? AND STATUS = 'Pending";
		List<ReimbursementRequest> requests = new ArrayList<>();
		
		ReimbursementRequest request = new ReimbursementRequest();
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql);){
			
			preparedStatement.setInt(1, employeeId);
			
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				
				request.setRequestId(rs.getInt("REQUEST_ID"));
				request.setRequestAmount(rs.getFloat("REQUEST_AMOUNT"));
				request.setPaidToFirstName(rs.getString("PAID_TO_FIRST_NAME"));
				request.setPaidToLastName(rs.getString("PAID_TO_LAST_NAME"));
				request.setReimbursedOn(rs.getString(rs.getString("REIMBURSED_ON")));
				request.setPaidThrough(rs.getString("PAID_THROUGH"));
				request.setReferenceNumber(rs.getInt("REFERENCE_NUMBER"));
				request.setStatus(rs.getString("STATUS"));
				request.setApprovedByFirstName(rs.getString("APPROVED_BY_FIRST_NAME"));
				request.setApprovedByLastName(rs.getString("APPROVED_BY_LAST_NAME"));
				request.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				
				requests.add(request);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return requests;
	}

	//This method gets all the resolved requests that a specific employee has
	@Override
	public List<ReimbursementRequest> GetAllResolvedRequestsByEmployeeId(int employeeId) {
		String sql = "SELECT * FROM REIMBURSEMENT_REQUEST_INFO WHERE EMPLOYEE_ID = ? AND STATUS = 'Resolved";
		List<ReimbursementRequest> requests = new ArrayList<>();
		
		ReimbursementRequest request = new ReimbursementRequest();
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(sql);){
			
			preparedStatement.setInt(1, employeeId);
			
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				
				request.setRequestId(rs.getInt("REQUEST_ID"));
				request.setRequestAmount(rs.getFloat("REQUEST_AMOUNT"));
				request.setPaidToFirstName(rs.getString("PAID_TO_FIRST_NAME"));
				request.setPaidToLastName(rs.getString("PAID_TO_LAST_NAME"));
				request.setReimbursedOn(rs.getString(rs.getString("REIMBURSED_ON")));
				request.setPaidThrough(rs.getString("PAID_THROUGH"));
				request.setReferenceNumber(rs.getInt("REFERENCE_NUMBER"));
				request.setStatus(rs.getString("STATUS"));
				request.setApprovedByFirstName(rs.getString("APPROVED_BY_FIRST_NAME"));
				request.setApprovedByLastName(rs.getString("APPROVED_BY_LAST_NAME"));
				request.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				
				requests.add(request);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return requests;
	}

	//This method grabs all the requests from all employees that are still pending
	@Override
	public List<ReimbursementRequest> GetAllPendingRequests() {
		String sql = "SELECT * FROM REIMBURSEMENT_REQUEST_INFO WHERE STATUS = 'Pending'";
		List<ReimbursementRequest> pendingRequests = new ArrayList<>();
		
		ReimbursementRequest pendingRequest = new ReimbursementRequest();
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement();){
			ResultSet rs = statement.executeQuery(sql);
			while(rs.next()) {
				
				pendingRequest.setRequestId(rs.getInt("REQUEST_ID"));
				pendingRequest.setRequestAmount(rs.getFloat("REQUEST_AMOUNT"));
				pendingRequest.setPaidToFirstName(rs.getString("PAID_TO_FIRST_NAME"));
				pendingRequest.setPaidToLastName(rs.getString("PAID_TO_LAST_NAME"));
				pendingRequest.setReimbursedOn(rs.getString(rs.getString("REIMBURSED_ON")));
				pendingRequest.setPaidThrough(rs.getString("PAID_THROUGH"));
				pendingRequest.setReferenceNumber(rs.getInt("REFERENCE_NUMBER"));
				pendingRequest.setStatus(rs.getString("STATUS"));
				pendingRequest.setApprovedByFirstName(rs.getString("APPROVED_BY_FIRST_NAME"));
				pendingRequest.setApprovedByLastName(rs.getString("APPROVED_BY_LAST_NAME"));
				pendingRequest.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				
				pendingRequests.add(pendingRequest);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return pendingRequests;
	}

	//This method grabs all the requests from all employees that are resolved
	@Override
	public List<ReimbursementRequest> GetAllResolvedRequests() {
		String sql = "SELECT * FROM REIMBURSEMENT_REQUEST_INFO WHERE STATUS = 'Resolved'";
		List<ReimbursementRequest> resolvedRequests = new ArrayList<>();
		
		ReimbursementRequest resolvedRequest = new ReimbursementRequest();
		try(Connection connection = ConnectionUtil.getConnection();
				Statement statement = connection.createStatement();){
			ResultSet rs = statement.executeQuery(sql);
			while(rs.next()) {
				
				resolvedRequest.setRequestId(rs.getInt("REQUEST_ID"));
				resolvedRequest.setRequestAmount(rs.getFloat("REQUEST_AMOUNT"));
				resolvedRequest.setPaidToFirstName(rs.getString("PAID_TO_FIRST_NAME"));
				resolvedRequest.setPaidToLastName(rs.getString("PAID_TO_LAST_NAME"));
				resolvedRequest.setReimbursedOn(rs.getString(rs.getString("REIMBURSED_ON")));
				resolvedRequest.setPaidThrough(rs.getString("PAID_THROUGH"));
				resolvedRequest.setReferenceNumber(rs.getInt("REFERENCE_NUMBER"));
				resolvedRequest.setStatus(rs.getString("STATUS"));
				resolvedRequest.setApprovedByFirstName(rs.getString("APPROVED_BY_FIRST_NAME"));
				resolvedRequest.setApprovedByLastName(rs.getString("APPROVED_BY_LAST_NAME"));
				resolvedRequest.setEmployeeId(rs.getInt("EMPLOYEE_ID"));
				
				resolvedRequests.add(resolvedRequest);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resolvedRequests;
	}

	//This method updates the status of the requests from either "Resolved" or "Rejected"
	@Override
	public boolean UpdateStatus(String status, int requestId) {
		String sql = "UPDATE REIMBURSEMENT_REQUEST_INFO SET STATUS = ? WHERE REQUEST_ID = ?";
		int activeUpdated = 0;
		try(Connection connection = ConnectionUtil.getConnection();
				PreparedStatement pStatement = connection.prepareStatement(sql)){
			pStatement.setString(1, status);
			pStatement.setInt(2, requestId);
			activeUpdated = pStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(activeUpdated==1) {
			return true;
		}
		return false;
	}

}
