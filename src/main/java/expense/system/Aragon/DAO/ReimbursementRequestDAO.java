package expense.system.Aragon.DAO;

import java.util.List;

import expense.system.Aragon.Model.ReimbursementRequest;

public interface ReimbursementRequestDAO {
	
	public ReimbursementRequest SubmitRequest(ReimbursementRequest rr);
	
	public List<ReimbursementRequest> GetAllPendingRequestsByEmployeeId(int employeeId);
	
	public List<ReimbursementRequest> GetAllResolvedRequestsByEmployeeId(int employeeId);
	
	public List<ReimbursementRequest> GetAllPendingRequests();
	
	public List<ReimbursementRequest> GetAllResolvedRequests();
	
	public boolean UpdateStatus(String status, int requestId);
	
}
