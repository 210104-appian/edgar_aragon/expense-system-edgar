package expense.system.Aragon.DAO;

import java.util.List;

import expense.system.Aragon.Model.Employee;

public interface EmployeeDAO {
	
	public Employee VerifyNameAndPassword(String username, String password);
	
	public List<Employee> GetAllEmployees();
	
	public Employee GetEmployeeInfoByID(int employeeID);
	
	public boolean UpdateUsername(String username, int employeeId);
		
	public boolean UpdatePassword(String password, int employeeId);
	
	public boolean UpdateEmail(String email, int employeeId);
	
	public boolean UpdateCountry(String country, int employeeId);
	
	public boolean UpdatePhoneNumber(String phoneNumber, int employeeId);
	
}
