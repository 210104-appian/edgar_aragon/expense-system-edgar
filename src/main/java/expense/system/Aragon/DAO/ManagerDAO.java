package expense.system.Aragon.DAO;

import expense.system.Aragon.Model.Manager;

public interface ManagerDAO {
	
	public boolean VerifyUsernameAndPassword(String username, String password);
	
	public Manager GetManagerInfoByID(int managerID);
	
}
