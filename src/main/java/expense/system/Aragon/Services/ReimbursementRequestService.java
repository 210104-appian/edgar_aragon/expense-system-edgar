package expense.system.Aragon.Services;

import java.util.List;

import expense.system.Aragon.DAO.ReimbursementRequestDAO;
import expense.system.Aragon.DAO.Impl.ReimbursementRequestDAOImpl;
import expense.system.Aragon.Model.ReimbursementRequest;

public class ReimbursementRequestService {
	
	ReimbursementRequestDAO rrdao = new ReimbursementRequestDAOImpl();
	
	public ReimbursementRequest SendRequest(ReimbursementRequest rr) {
		ReimbursementRequest requestSent = rrdao.SubmitRequest(rr);
		return requestSent;
	}
	
	public List<ReimbursementRequest> AllPendingRequestsByEmployeeId(int employeeId)
	{
		List<ReimbursementRequest> pendingRequests = rrdao.GetAllPendingRequestsByEmployeeId(employeeId);
		return pendingRequests;
	}
	
	public List<ReimbursementRequest> AllResolvedRequestsByEmployeeId(int employeeId)
	{
		List<ReimbursementRequest> reslovedRequests = rrdao.GetAllResolvedRequestsByEmployeeId(employeeId);
		return reslovedRequests;
	}
	
	public List<ReimbursementRequest> AllPendingRequests()
	{
		List<ReimbursementRequest> pendingRequests = rrdao.GetAllPendingRequests();
		return pendingRequests;
	}
	
	public List<ReimbursementRequest> AllResolvedRequests() {
		List<ReimbursementRequest> reslovedRequests = rrdao.GetAllResolvedRequests();
		return reslovedRequests;
	}
	
	public boolean ChangeStatus(String status, int requestId) {
		boolean statusChanged = rrdao.UpdateStatus(status, requestId);
		return statusChanged;
	}

}
