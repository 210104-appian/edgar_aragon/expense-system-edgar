package expense.system.Aragon.Services;

import expense.system.Aragon.DAO.ManagerDAO;
import expense.system.Aragon.DAO.Impl.ManagerDAOImpl;
import expense.system.Aragon.Model.Manager;

public class ManagerService {

	ManagerDAO mdao = new ManagerDAOImpl();
	
	public boolean CheckNameAndPassword(String username, String password) {
		boolean nameAndPasswordAccepted = mdao.VerifyUsernameAndPassword(username, password);
		return nameAndPasswordAccepted;
	}
	
	public Manager ManagerInfoByID(int managerID)
	{
		Manager infoRecieved = mdao.GetManagerInfoByID(managerID);
		return infoRecieved;
	}
}
