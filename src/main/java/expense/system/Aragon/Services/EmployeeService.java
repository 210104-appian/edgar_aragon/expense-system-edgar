package expense.system.Aragon.Services;

import java.util.List;

import expense.system.Aragon.DAO.EmployeeDAO;
import expense.system.Aragon.DAO.Impl.EmployeeDAOImpl;
import expense.system.Aragon.Model.Employee;

public class EmployeeService {
	
	EmployeeDAO edao = new EmployeeDAOImpl();
	
	public Employee CheckNameAndPassword(String username, String password) {
		Employee nameAndPasswordAccepted = edao.VerifyNameAndPassword(username, password);
		return nameAndPasswordAccepted;
	}
	
	public List<Employee> AllEmployees()
	{
		List<Employee> employees = edao.GetAllEmployees();
		return employees;
	}
	
	public Employee employeeInfoByID(int employeeId)
	{
		Employee employee = edao.GetEmployeeInfoByID(employeeId);
		return employee;
	}
	
	public boolean ChangeUsername(String username, int employeeId)
	{
		boolean firstNameUpdated = edao.UpdateUsername(username, employeeId);
		return firstNameUpdated;
	}
	
	public boolean ChangePassword(String password, int employeeId)
	{
		boolean passwordUpdated = edao.UpdatePassword(password, employeeId);
		return passwordUpdated;
	}
	
	public boolean ChangeEmail(String email, int employeeId)
	{
		boolean emailUpdated = edao.UpdateEmail(email, employeeId);
		return emailUpdated;
	}
	
	public boolean ChangeCountry(String country, int employeeId)
	{
		boolean countryUpdated = edao.UpdateCountry(country, employeeId);
		return countryUpdated;
	}
	
	public boolean ChangePhoneNumber(String phoneNumber, int employeeId)
	{
		boolean phoneNumberUpdated = edao.UpdatePhoneNumber(phoneNumber, employeeId);
		return phoneNumberUpdated;
	}
}
