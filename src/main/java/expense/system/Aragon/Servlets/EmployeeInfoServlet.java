package expense.system.Aragon.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import expense.system.Aragon.Model.Employee;
import expense.system.Aragon.Services.EmployeeService;

public class EmployeeInfoServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	EmployeeService es = new EmployeeService();
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		
		Employee employee = new Employee();
		
		ObjectMapper om = new ObjectMapper();
		
		Employee credentials = om.readValue(request.getReader().readLine(), Employee.class);
		
		if(employee !=null) {
			employee = es.employeeInfoByID(credentials.getEmployeeId());
		}
		
		String itemsJson = om.writeValueAsString(employee);
		PrintWriter pw = response.getWriter();
		pw.write(itemsJson);
		pw.close();

	}

}
