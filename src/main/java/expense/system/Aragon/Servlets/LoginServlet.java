package expense.system.Aragon.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import expense.system.Aragon.Model.Employee;
import expense.system.Aragon.Services.EmployeeService;

public class LoginServlet extends HttpServlet {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	EmployeeService es = new EmployeeService();

	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		request.getRemoteHost();
		
		ObjectMapper om = new ObjectMapper();
		
		Employee credentials = om.readValue(request.getReader().readLine(), Employee.class);
		
		Employee employee = es.CheckNameAndPassword(credentials.getUsername(), credentials.getPassword());
		
		if(employee != null) {
			String token = employee.getUsername() + ":" + employee.getPassword();
			try(PrintWriter pw = response.getWriter()){
				pw.write(token);
			}
		} else {
			response.sendError(401);
		}

	}
	
}
