package expense.system.Aragon.Servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import expense.system.Aragon.Model.Manager;
import expense.system.Aragon.Services.ManagerService;

public class ManagerInfoServlet extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	ManagerService ms = new ManagerService();
	
protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException {
		
		Manager manager = new Manager();
		
		ObjectMapper om = new ObjectMapper();
		
		Manager credentials = om.readValue(request.getReader().readLine(), Manager.class);
		
		if(manager !=null) {
			manager = ms.ManagerInfoByID(credentials.getManagerId());
		}
		
		String itemsJson = om.writeValueAsString(manager);
		PrintWriter pw = response.getWriter();
		pw.write(itemsJson);
		pw.close();

	}
}
