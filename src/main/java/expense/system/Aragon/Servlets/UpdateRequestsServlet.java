package expense.system.Aragon.Servlets;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import expense.system.Aragon.Model.ReimbursementRequest;
import expense.system.Aragon.Services.ReimbursementRequestService;

public class UpdateRequestsServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
private ReimbursementRequestService rrs = new ReimbursementRequestService();
	
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		BufferedReader bw = request.getReader();
		String json = "";
		String line = bw.readLine();
		while(line!=null) {
			json = json + line;
			line = bw.readLine();
		}
		
		ObjectMapper om = new ObjectMapper();
		ReimbursementRequest updatedRequest = om.readValue(json, ReimbursementRequest.class);
		
		boolean requestChanged = rrs.ChangeStatus(updatedRequest.getStatus(), updatedRequest.getRequestId());

		if(requestChanged) {
			
			response.setStatus(201);
			
		} else {
			
			response.sendError(400);
		}
	}

}
