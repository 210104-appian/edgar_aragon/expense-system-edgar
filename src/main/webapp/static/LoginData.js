const baseUrl = "http://localhost:8080/Expense-Express-Aragon";

const loginUrl = baseUrl + "/login";

function performAjaxGetRequest(url, callback){
    const xhr = new XMLHttpRequest();
    xhr.open("GET", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4){
            if(xhr.status==200){
                callback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                console.error("something went wrong with our GET request to "+url);
            }
        } 
    }
    xhr.send();
}

function performAjaxPostRequest(url, payload, successCallback, failureCallback){
    const xhr = new XMLHttpRequest();
    xhr.open("POST", url);
    xhr.onreadystatechange = function(){
        if(xhr.readyState==4 ){
            if(xhr.status>199 && xhr.status<300){
                successCallback(xhr.response); // this is going to be the response body of our http response  (JSON)
            } else {
                if(failureCallback){
                    failureCallback()
                } else{
                    console.error("An error occurred while attempting to create a new record")
                }
            }
        }
    }
    xhr.send(payload);
}
