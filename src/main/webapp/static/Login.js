document.getElementById("login-nav-opt").addEventListener("click", openModal);
document.getElementById("close-btn").addEventListener("click", closeModal);
document.getElementById("submit-btn").addEventListener("click", attemptLogin);

function openModal(){
    $('#loginModal').modal('show');
}

function closeModal(){
    $('#loginModal').modal('hide');
}

function attemptLogin(){
    let username = document.getElementById("username-input").value;
    let password = document.getElementById("password-input").value;
    let credentials = {username, password};
    // console.log(credentials);
    const loginUrl = "http://localhost:8081/Expense.System.Aragon/login";
    performAjaxPostRequest(loginUrl, JSON.stringify(credentials), handleSuccessfulLogin, handleUnsuccessfulLogin);
}

function handleSuccessfulLogin(responseText){
    console.log("Success! You're logged in");
    document.getElementById("error-msg").hidden = true;
    closeModal();
    let token = responseText;
    sessionStorage.setItem("token", token);
    toggleLoginToLogout();
    displayLoggedInUser();
}

function handleUnsuccessfulLogin(){
    console.log("Login unsuccessful");
    document.getElementById("error-msg").hidden = false;
}

function toggleLoginToLogout(){
    let loginBtn = document.getElementById("login-nav-opt");
    loginBtn.innerText = "Log out";
    loginBtn.removeEventListener("click", openModal);
    loginBtn.addEventListener("click", logout);
}

function logout(){
    sessionStorage.removeItem("token");
    toggleLogoutToLogin();
    removeLoggedInUserGreeting();
}

function toggleLogoutToLogin(){
    let loginBtn = document.getElementById("login-nav-opt");
    loginBtn.innerText = "Log in";
    loginBtn.addEventListener("click", openModal);
    loginBtn.removeEventListener("click", logout);
}

/*
window.onload = function(){
    let token = sessionStorage.getItem("token");
    // if this token is null, redirect (on the frontend, change the browsers location - perhaps to some web page that allows us to login)
    // send an ajax request validating the token
    // if token valid? stay on page
    // if token is invalid, redirect (on the frontend, change the browsers location)
}
*/

function displayLoggedInUser(){
    const token = sessionStorage.getItem("token");
    // ex. 1:ADMIN
    const parsedToken = token.split(":");
    const loggedInID = parsedToken[0];
    getUserById(loggedInID, function(usersJSON){
        const user = JSON.parse(usersJSON);
        // document.getElementById("greeting-header").innerText = "Welcome to Revature Bakery, "+ user.firstName + " " + user.lastName;
        document.getElementById("greeting-header").innerText = `Welcome to Revature Bakery, ${user.firstName} ${user.lastName}`;
    });
}

function removeLoggedInUserGreeting(){
    document.getElementById("greeting-header").innerText = `Welcome to Revature Bakery`;

}