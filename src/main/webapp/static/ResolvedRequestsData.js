const stringResolvedRequests = `[{"id":5,"price":2.5,"type":"BAGUETTE"},{"id":6,"price":3.0,"type":"SOURDOUGH"},{"id":7,"price":2.75,"type":"PUMPERNICKEL"}]`;

const pendingRequests = JSON.parse(stringResolvedRequests);

window.onload = function(){

    addRows(resolvedRequests,"table-body");
}

function addRows(values, id)
{
    for(let val of values){
        let row = document.createElement("tr");

        let dateData = document.createElement("td");
        dateData.innerText = val.id;
        let referenceData = document.createElement("td");
        referenceData.innerText = val.id;
        let paidThroughData = document.createElement("td");
        paidThroughData.innerText = val.id;
        let paidToData = document.createElement("td");
        paidToData.innerText = val.id;
        let statusData = document.createElement("td");
        statusData.innerText = val.id;
        let amountData = document.createElement("td");
        amountData.innerText = val.id;
        row.append(dateData, referenceData, paidThroughData, paidToData, statusData, amountData);
        let parent = document.getElementById(id);
        parent.append(row);
    }
}
