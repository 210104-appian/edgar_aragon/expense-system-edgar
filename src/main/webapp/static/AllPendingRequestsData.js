const stringAllPendingRequests = `[{"id":5,"price":2.5,"type":"BAGUETTE"},{"id":6,"price":3.0,"type":"SOURDOUGH"},{"id":7,"price":2.75,"type":"PUMPERNICKEL"}]`;

const allPendingRequests = JSON.parse(stringAllPendingRequests);

window.onload = function(){

    addRows(allPendingRequests,"table-body");
}

function addRows(values, id)
{
    for(let val of values){
        let row = document.createElement("tr");

        let requestIdData = document.createElement("td");
        requestIdData.innerText = val.id;
        let transactionDateData = document.createElement("td");
        transactionDateData.innerText = val.id;
        let referenceNumberData = document.createElement("td");
        referenceNumberData.innerText = val.id;
        let paidThroughData = document.createElement("td");
        paidThroughData.innerText = val.id;
        let paidToData = document.createElement("td");
        paidToData.innerText = val.id;
        let statusData = document.createElement("td");
        statusData.innerText = val.id;
        let amountData = document.createElement("td");
        amountData.innerText = val.id;
        let employeeIdData = document.createElement("td");
        employeeIdData.innerText = 0;
        row.append(requestIdData, transactionDateData, referenceNumberData, paidThroughData, paidToData, statusData, amountData, employeeIdData);
        let parent = document.getElementById(id);
        parent.append(row);
    }
}