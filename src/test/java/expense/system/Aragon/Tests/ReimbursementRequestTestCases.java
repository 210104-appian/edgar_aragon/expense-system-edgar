package expense.system.Aragon.Tests;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.List;

import org.junit.Test;

import expense.system.Aragon.DAO.ReimbursementRequestDAO;
import expense.system.Aragon.DAO.Impl.ReimbursementRequestDAOImpl;
import expense.system.Aragon.Model.ReimbursementRequest;

public class ReimbursementRequestTestCases {
	
	ReimbursementRequestDAO rrdao = new ReimbursementRequestDAOImpl();
	
	@Test
	public void testGetAllPendingRequestsByEmployeeIdWithNoValue()
	{
		List<ReimbursementRequest> expected = Collections.emptyList();
		List<ReimbursementRequest> actual = rrdao.GetAllPendingRequestsByEmployeeId(0);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetAllResolvedRequestsByEmployeeIdWithZeroValue()
	{
		List<ReimbursementRequest> expected = Collections.emptyList();
		List<ReimbursementRequest> actual = rrdao.GetAllResolvedRequestsByEmployeeId(0);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetAllPendingRequestsByEmployeeIdWithValue()
	{
		List<ReimbursementRequest> expected = Collections.emptyList();
		List<ReimbursementRequest> actual = rrdao.GetAllResolvedRequestsByEmployeeId(3);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetAllPendingRequests()
	{
		List<ReimbursementRequest> expected = Collections.emptyList();
		List<ReimbursementRequest> actual = rrdao.GetAllPendingRequests();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testGetAllResolvedRequests()
	{
		List<ReimbursementRequest> expected = Collections.emptyList();
		List<ReimbursementRequest> actual = rrdao.GetAllResolvedRequests();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdateStatusWithValues()
	{
		boolean expected = false;
		boolean actual = rrdao.UpdateStatus("Pending", 1);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdateStatusWithNullValues()
	{
		boolean expected = false;
		boolean actual = rrdao.UpdateStatus(null, 0);
		assertEquals(expected, actual);
	}

}
