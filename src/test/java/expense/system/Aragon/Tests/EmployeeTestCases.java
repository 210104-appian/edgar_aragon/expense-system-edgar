package expense.system.Aragon.Tests;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.List;

import org.junit.Test;

import expense.system.Aragon.DAO.EmployeeDAO;
import expense.system.Aragon.DAO.Impl.EmployeeDAOImpl;
import expense.system.Aragon.Model.Employee;

public class EmployeeTestCases {
	
	EmployeeDAO edao = new EmployeeDAOImpl();
	Employee employee = new Employee();
	
	@Test
	public void testGetAllEmployees()
	{
		List<Employee> expected = Collections.emptyList();
		List<Employee> actual = edao.GetAllEmployees();
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdatePasswordWithNoValues()
	{
		boolean expected = false;
		boolean actual = edao.UpdatePassword(null, 0);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdatePasswordValues()
	{
		boolean expected = false;
		boolean actual = edao.UpdatePassword("JanFeb", 1);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdateEmailWithNoValues()
	{
		boolean expected = false;
		boolean actual = edao.UpdateEmail(null, 0);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdateEmailValues()
	{
		boolean expected = false;
		boolean actual = edao.UpdateEmail("Hunt456@GMAIL.COM", 1);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdateContryWithNoValues()
	{
		boolean expected = false;
		boolean actual = edao.UpdateCountry(null, 0);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdateCountryValues()
	{
		boolean expected = false;
		boolean actual = edao.UpdateCountry("Mexico", 1);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdatePhoneNumberWithNoValues()
	{
		boolean expected = false;
		boolean actual = edao.UpdateCountry(null, 0);
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUpdatePhoneNumberValues()
	{
		boolean expected = false;
		boolean actual = edao.UpdateCountry("678-902-7311", 1);
		assertEquals(expected, actual);
	}
}
